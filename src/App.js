import React, { Component } from 'react';

import {BrowserRouter,Route, Switch} from 'react-router-dom'; 
import { NavLink } from 'react-router-dom';
import Home from './Home';
import Contact from './Contact';
import Nav from './Nav';



class App extends Component {
  render() {
    return (
      
   
      <BrowserRouter>
      <div>
     
         <Nav />
          <Switch>
              <Route path="/" render={Home} exact />
           
              <Route path="/Contact" render={Contact} />
              <Route component={Error} />
            </Switch>
       </div>
      </BrowserRouter>

      );

   
  }
  
}



export default App;
